User-agent: *
Disallow: /logs
Disallow: /logs.py
Disallow: /top10
Disallow: /top10.py
Disallow: /traffic
Disallow: /traffic.html
Disallow: /qes.csv
Disallow: /reqs.csv

# Block OpenAI
User-agent: GPTBot
Disallow: /
User-agent: ChatGPT-User
Disallow: /

# Block Google Bard AI
User-agent: Google-Extended
Disallow: /

# Block Common Crawl
User-agent: CCBot
Disallow: /

Sitemap: https://lmddgtfy.net/sitemap.xml
